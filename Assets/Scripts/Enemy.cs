﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] float currentSpeed = 1f;

    private void Awake() {
        //FindObjectOfType<LevelController>().AttackerSpawned();
    }

    private void OnDestroy() {
        /*
        LevelController levelController = FindObjectOfType<LevelController>();
        if (levelController != null) {
            levelController.AttackerKilled();
        }
        */
    }

    void Update() {
        transform.Translate(Vector2.left * currentSpeed * Time.deltaTime);
    }

    public void SetMovementSpeed(float speed) {
        currentSpeed = speed;
    }

}
