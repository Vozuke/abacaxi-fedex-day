﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerPlatform : MonoBehaviour {

    [Header("Status")]
    [SerializeField] int fullHealth = 3;
    [SerializeField] float currentHealth = 3;

    [Header("Settings")]
    [SerializeField] float immortalTimeAfterDamage = 2f;
    [SerializeField] bool immortalActive = false;
    [SerializeField] float timeAfterDefeat = 5f;
    [SerializeField] public HealthBar healthBar;
    [SerializeField] LayerMask enemyLayers;

    [Header("Sound effects")]
    [SerializeField] AudioClip[] hurtSFX;

    //State
    bool isAlive = true;

    //Cached component refences
    Rigidbody2D rigidBody;
    Animator animator;
    CapsuleCollider2D bodyCollider2D;
    private Collider2D controllerCollider;

    // Start is called before the first frame update
    void Start() {
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        bodyCollider2D = GetComponent<CapsuleCollider2D>();

        controllerCollider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update() {
        if (!isAlive) {
            return;
        }

        Move();

        DamageTakenFromHazard();

    }

    private void Move() {
        animator.SetBool("Walk", false);
    }

    private void DamageTakenFromHazard() {


        if (bodyCollider2D.IsTouchingLayers(LayerMask.GetMask("Enemy", "Hazard"))) {
            if (immortalActive) {
                return;
            }

            immortalActive = true;

            AudioManager.PlaySound(hurtSFX[Random.Range(0, hurtSFX.Length)]);
            StartCoroutine(ImmortalActivation(immortalTimeAfterDamage));

            healthBar.ChangeHealthBar(currentHealth, fullHealth);

            if (currentHealth <= 0) {
                isAlive = false;
                StartCoroutine(Defeat());
            }
        }
    }


    private IEnumerator Defeat() {

        //Animation and halting
        animator.SetTrigger("defeat");

        yield return new WaitForSeconds(timeAfterDefeat);

        //you died, and fade in screen 
        FindObjectOfType<InGameCanvas>().ResetGameSession();
    }

    private IEnumerator ImmortalActivation(float immortalTimeAfterDamage) {
        yield return new WaitForSeconds(immortalTimeAfterDamage);
        immortalActive = false;
    }

}
