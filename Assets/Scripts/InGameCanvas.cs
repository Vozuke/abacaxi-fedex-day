﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameCanvas : MonoBehaviour {

    [SerializeField] int score = 0;
    [SerializeField] Text scoreText;

    void Start() {
        scoreText.text = score.ToString();
    }

    public void AddToScore(int pointsToAdd) {
        score += pointsToAdd;
        scoreText.text = score.ToString();
    }

    public void ResetGameSession() {
        SceneManager.LoadScene(0);
        Destroy(gameObject);
    }

}
