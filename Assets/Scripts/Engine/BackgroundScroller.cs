﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BackgroundScroller : MonoBehaviour {

    [SerializeField] float backgroundScrollSpeed = 0.1f;
    Material material;
    Vector2 offSet;


    void Start() {
        material = GetComponent<Renderer>().material;
        offSet = new Vector2(backgroundScrollSpeed, 0f);
    }

    void Update() {
        material.mainTextureOffset += offSet * Time.deltaTime;
    }
}
