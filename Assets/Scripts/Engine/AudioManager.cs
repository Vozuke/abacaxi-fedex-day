﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    static AudioSource[] audioSource;
    [SerializeField] AudioClip music;
    [SerializeField] AudioClip defeat;
    [SerializeField] AudioClip victory;

    bool currentlyPlayingMusic = false;
    bool currentlyPlayingEnvironment= false;

    void Awake() {
        SetUpSingleton();
    }

    private void SetUpSingleton() {
        if (FindObjectsOfType(GetType()).Length > 1) {
            Destroy(gameObject);
        } else {
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start() {
        audioSource = GetComponents<AudioSource>();

        audioSource[0].clip = music;
    }

    void Update() {
        if (currentlyPlayingMusic == false && GameSettings.musicVolume > 0) {
            audioSource[0].Play();
            currentlyPlayingMusic = true;
        } else if (currentlyPlayingMusic == true && GameSettings.musicVolume <= 0) {
            audioSource[0].Stop();
            currentlyPlayingMusic = false;
        }

    }

    public static void PlaySound(AudioClip audioClip) {
        audioSource[1].PlayOneShot(audioClip, GameSettings.soundVolume);
    }

    public void ChangeMusic() {

    }

    public void ChangeVolume(float newValue) {
        if (audioSource != null && audioSource.Length >= 2 && audioSource[0] != null) {
            audioSource[0].volume = newValue;
        }
    }

    public void ChangeVolumeEnvironment(float newValue) {
        if (audioSource != null && audioSource.Length >= 2 && audioSource[2] != null) {
            audioSource[2].volume = newValue;
        }
    }
}
