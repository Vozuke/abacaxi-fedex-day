﻿using UnityEngine;

public class GameSettings : MonoBehaviour {

    public static float musicVolume;
    public static float soundVolume;

    void Awake() {
        SetUpSingleton();
        InitialSetup();
    }

    private void SetUpSingleton() {
        if (FindObjectsOfType(GetType()).Length > 1) {
            Destroy(gameObject);
        } else {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void InitialSetup() {
        musicVolume = PlayerPrefs.GetFloat("musicVolume", 0.7f);
        soundVolume = PlayerPrefs.GetFloat("soundVolume", 1f);
    }

}
