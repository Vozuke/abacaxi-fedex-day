﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOptions : MonoBehaviour {

    private float musicVolume;
    private float soundVolume;

    void Start() {
        InitialSetup();
    }

    public void InitialSetup() {
        musicVolume = GameSettings.musicVolume;
        OnMusicValueChanged(musicVolume);

        soundVolume = GameSettings.soundVolume;
        OnSoundValueChanged(soundVolume);
    }

    public void OnMusicValueChanged(float newValue) {
        PlayerPrefs.SetFloat("musicVolume", newValue);
        GameSettings.musicVolume = newValue;
        FindObjectOfType<AudioManager>().ChangeVolume(newValue);
    }

    public void OnSoundValueChanged(float newValue) {
        PlayerPrefs.SetFloat("soundVolume", newValue);
        GameSettings.soundVolume = newValue;
    }

}
