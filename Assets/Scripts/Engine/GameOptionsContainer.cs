﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOptionsContainer : MonoBehaviour {

    [SerializeField] Slider music;
    [SerializeField] Slider sound;

    void Awake() {
        CofigureOptions();
    }

    public void CofigureOptions() {
        music.value = GameSettings.musicVolume;
        sound.value = GameSettings.soundVolume;
    }

}
