﻿using UnityEngine;
using UnityEngine.InputSystem;

public class InGameMenu : MonoBehaviour {

    public static bool isGamePaused = false;

    public GameObject pauseMenuUI;

    private bool inGameMenuOpen = false;
    private bool inGameOptionsOpen = false;

    [SerializeField] GameObject inGameMenu;
    [SerializeField] GameObject inGameOptions;
    [SerializeField] AudioClip openCloseSound;

    private void Start() {
        inGameMenu.SetActive(false);
        inGameOptions.SetActive(false);
    }

    void Update() {

        var keyboard = Keyboard.current;

        if (keyboard.escapeKey.wasPressedThisFrame) {
            
            if (!inGameMenuOpen && !inGameOptionsOpen) {
                Pause();
            } else if (inGameMenuOpen && !inGameOptionsOpen) {
                HideMenu();
            }
        }
       
    }

    public void Resume() {
        inGameMenuOpen = !inGameMenuOpen;
        Time.timeScale = 1f;
        HideMenu();
    }

    public void OpenOptions() {
        HideMenu();
        inGameOptionsOpen = true;
        inGameOptions.SetActive(inGameOptionsOpen);
    }

    public void CloseOptions() {
        Pause();
        inGameOptionsOpen = false;
        inGameOptions.SetActive(inGameOptionsOpen);
    }

    public void HideMenu() {
        inGameMenu.SetActive(false);
        inGameMenuOpen = false;
        PlaySoundOnHover();
    }

    public void Pause() {
        inGameMenu.SetActive(true);
        inGameMenuOpen = true;
        Time.timeScale = 0f;
        PlaySoundOnHover();
    }


    private void PlaySoundOnHover() {
        AudioManager.PlaySound(openCloseSound);
    }
}