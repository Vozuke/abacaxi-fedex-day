﻿using UnityEngine;

public class Menu : MonoBehaviour {

    [SerializeField] AudioClip hoverSound;

    public void StartFirstScene() {
        FindObjectOfType<LevelLoader>().LoadLevel(1);
        SoundOnMouseEnter();
    }

    public void GoToMenu() {
        FindObjectOfType<LevelLoader>().LoadLevel(0);
        SoundOnMouseEnter();
        FindObjectOfType<InGameMenu>().HideMenu();
    }

    public void ExitGame() {
        Application.Quit();
        SoundOnMouseEnter();
    }

    public void ResumeGame() {
        FindObjectOfType<InGameMenu>().Resume();
        SoundOnMouseEnter();
    }

    public void ToggleOptions() {
        FindObjectOfType<InGameMenu>().OpenOptions();
        FindObjectOfType<GameOptionsContainer>().CofigureOptions();
        SoundOnMouseEnter();
    }

    public void CloseOptions() {
        FindObjectOfType<InGameMenu>().CloseOptions();
        SoundOnMouseEnter();
    }

    public void SoundOnMouseEnter() {
        AudioManager.PlaySound(hoverSound);
    }

}
