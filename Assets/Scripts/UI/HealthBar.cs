﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour{

    public Image lifeBarImage;
    public Gradient gradient;

    public void ChangeHealthBar(float value, float total) {
        lifeBarImage.fillAmount = value / total;
        lifeBarImage.color = gradient.Evaluate(value / total);
    }

}
